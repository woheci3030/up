<?php
class ControllerReportFarmerreport extends Controller {
   public function  index(){
        ini_set('display_errors',1);
        ini_set('display_startup_errors',1);
        error_reporting(0);
        ini_set('memory_limit', '500M');

        $this->load->language('report/report');
        $this->load->model('report/farmerreport');
        $data['text_list'] = $this->language->get('text_list');
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_model'] = $this->language->get('entry_model');
        $data['entry_price'] = $this->language->get('entry price');

        if (isset($this->session->data['error'])) {
                $data['error_warning'] = $this->session->data['error'];
                unset($this->session->data['error']);
        } 
        elseif (isset($this->error['warning'])) {
                $data['error_warning'] = $this->error['warning'];
        } 
        else {
                $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];

                unset($this->session->data['success']);
        } else {
                $data['success'] = '';
        }
        $this->getList();
    }
    
    protected function getList() {
        $data['createdby'] = $this->model_report_farmerreport->getCreatedBy();
        $data['dpzone'] = $this->model_report_farmerreport->getZone();
        $data['frmdata'] =$this->model_report_farmerreport->getFarmerList($this->request->post);
        $data['heading_title'] = $this->language->get('heading_title');
        $data['token'] = $this->session->data['token'];
        $this->load->language('report/report');
	$this->document->setTitle("Farmer Report");
      	$this->load->model('report/farmerreport');
      	$data['entry_status'] = $this->language->get('Search Village Name');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }
        $data['order_statuses'] = "";
        $this->session->data["title"]=$this->language->get('heading_title');  
        $data['searchvillage'] = $this->url->link('report/farmerreport', 'token=' . $this->session->data['token'], 'SSL');
                
            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                    'text' => $this->language->get('text_home'),
                    'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                    'text' => $this->language->get('heading_title'),
                    'href' => $this->url->link('report/farmerreport', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['button_save'] = $this->language->get('button_save');
            $data['button_back'] = $this->language->get('button_back');
            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');
            $this->response->setOutput($this->load->view('report/farmerreport.tpl', $data));
         
     }
    public function download(){
       ini_set('display_errors',1);
       ini_set('display_startup_errors',1);
       error_reporting(-1);
       ini_set('memory_limit', '400M');
            
   	// Starting the PHPExcel library
    	$this->load->library('PHPExcel');
    	$this->load->library('PHPExcel/IOFactory');
    	$objPHPExcel = new PHPExcel();
    	$objPHPExcel->createSheet();
    	$objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
	$objPHPExcel->setActiveSheetIndex(0);
	// Field names in the first row
    	$fields = array(
                
                'FARMER_NAME',     
                'FATHER_NAME',      
                'MOBILE_NO',       
                //'EMAIL_ID',
                'VILLAGE',
                'ZONE',
                'STATE',
                'REGION',
                'DISTRICT',
                'HEAD QUARTER',
               // 'PIN CODE',    
                'ACERAGE', 
                'KHARIF CROP1', 
                'KHARIF ACRE1',
                'KHARIF CROP2', 
                'KHARIF ACRE2', 
                'KHARIF CROP3', 
                'KHARIF ACRE3',
                'RABI   CROP1',
                'RABI   ACRE1',
                'RABI   CROP2',
                'RABI   ACRE2',
                'RABI   CROP3',
                'RABI   ACRE3',    
                'DATE', 
                'DEALER',
                'CREATED BY',
                'GENERAL MANAGER',
                'ZONAL MANAGER',
                'REGIONAL MANAGER',
                'REGIONAL DEVELOPMENT MANAGER',
                'TERRITORY MANAGER',
                'AREA MANAGER'
                //'FARMER RATING',
               // 'PARTNER NAME'   
                 
             );
   
    	$col = 0;
    	foreach ($fields as $field){
        	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
       		$col++;
    	}
     
        $fileIO = fopen('php://memory', 'w+');
        fputcsv($fileIO, $fields,',');
    	$this->load->model('report/farmerreport');
      	$results = $this->model_report_farmerreport->getFarmerList($this->request->get);
	foreach($results as $data){
            $date=date_create($data['CR_DATE']);
            $cr_date= date_format($date,"Y-m-d");
            $fdata=array(
                       
                        $data['FARMER_NAME'],
                        $data['FATHER_NAME'],
                        $data['MOBILE_NO'],
                       // $data['EMAIL_ID'],
                        $data['VILLAGE_NAME'],
                        $data['ZONE_NAME'],
                        $data['STATE_NAME'],
                        $data['REGION_NAME'],
                        $data['DISTRICT_NAME'],
                        $data['HQ_NAME'],
                        //$data['PIN_CODE'],
                        $data['ACERAGE'],
                        $data['CROP1'],
                        $data['KHARIF_ACRE_1'],
                        $data['CROP2'],
                        $data['KHARIF_ACRE_2'],
                        $data['CROP3'],
                        $data['KHARIF_ACRE_3'],
                        $data['RAVI_CROP1'],
                        $data['RABI_ACRE_1'],
                        $data['RAVI_CROP2'],
                        $data['RABI_ACRE_2'],
                        $data['RAVI_CROP3'],
                        $data['RABI_ACRE_3'],
                        $cr_date,
                        $data['FIRM_NAME'],
                        $data['CR_NAME'],
                        $data['GM_NAME'],
                        $data['ZM_NAME'],
                        $data['RM_NAME'],
                        $data['RDM_NAME'],
                        $data['TM_NAME'],
                        $data['AM_NAME']
                        //$data['FARMER_RATING'],
                        //$data['WW_PARTNER_NAME']
                       
                          
            );
             fputcsv($fileIO,  $fdata,",");
   }
    fseek($fileIO, 0);
    PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
    PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment;filename="farmer_report_'.date('dMy').'.csv"');
    header('Cache-Control: max-age=0');
    fpassthru($fileIO);  
    fclose($fileIO);
    }
  /*****************************GEO DROPDOWN******************/
    public function getState($data){
       $this->load->model('report/farmerreport');
       if (isset($this->request->post['zone'])) {         
          $data['zone'] =$this->model_report_farmerreport->getState($this->request->post); 
          $dparea=   count($data['zone']);
          echo ' <option  value=""> Select State</option> ';
            for($n=0;$n<$dparea;$n++)
                {
                  echo '<option value="'.$data['zone'][$n]['SID'].'">'.ucfirst($data['zone'][$n]['GEO_NAME']).'</option>';
                }  
       }    
    } 
    
    public function getRegion($data){
       $this->load->model('report/farmerreport');
       if (isset($this->request->post['state'])) {         
          $data['zone'] =$this->model_report_farmerreport->getRegion($this->request->post); 
          $dparea=   count($data['zone']);
          echo ' <option  value=""> Select Region</option> ';
            for($n=0;$n<$dparea;$n++)
                {
                  echo '<option value="'.$data['zone'][$n]['SID'].'">'.ucfirst($data['zone'][$n]['GEO_NAME']).'</option>';
                }  
          }    
    }
    
    public function getTerritory($data){
       $this->load->model('report/farmerreport');
       if (isset($this->request->post['region'])) {         
          $data['zone'] =$this->model_report_farmerreport->getTerritory($this->request->post); 
          $dparea=   count($data['zone']);
          echo ' <option  value=""> Select Territory</option> ';
            for($n=0;$n<$dparea;$n++)
                {
                  echo '<option value="'.$data['zone'][$n]['SID'].'">'.ucfirst($data['zone'][$n]['GEO_NAME']).'</option>';
                }  
       }    
    }
    
    public function getDistrict($data){
       $this->load->model('report/farmerreport');
       if (isset($this->request->post['territory'])) {         
          $data['zone'] =$this->model_report_farmerreport->getDistrict($this->request->post); 
          $dparea=   count($data['zone']);
          echo ' <option  value=""> Select District</option> ';
            for($n=0;$n<$dparea;$n++)
                {
                  echo '<option value="'.$data['zone'][$n]['SID'].'">'.ucfirst($data['zone'][$n]['GEO_NAME']).'</option>';
                }  
       }    
    }
    
    public function getArea($data){
       $this->load->model('report/farmerreport');
       if (isset($this->request->post['district'])) {         
          $data['zone'] =$this->model_report_farmerreport->getHq($this->request->post); 
          $dparea=   count($data['zone']);
          echo ' <option  value=""> Select Head Quarter</option> ';
            for($n=0;$n<$dparea;$n++)
                {
                  echo '<option value="'.$data['zone'][$n]['SID'].'">'.ucfirst($data['zone'][$n]['GEO_NAME']).'</option>';
                }  
       }    
    }
  public function getFrmData(){
      $this->load->model('report/farmerreport'); 
      $Frmdata =$this->model_report_farmerreport->getFarmerList($this->request->post); 
        $i=1;
        $str='';
        $str.='<div class="table-responsive"  width="100%">';
        $str.='<table class="table display" id="example" border="1">';
        $str.='<thead>';
        $str.='<tr style="background: #3a8b8d; color: #ffffff !important;">';
        $str.='<th class="text-center" style="font-weight: bold;width:5%">S.No.</th>';
        $str.='<th class="text-center" style="font-weight: bold;">FARMER NAME</th>';
        $str.='<th class="text-center" style="font-weight: bold;">FATHER NAME</th>';
        $str.='<th class="text-center" style="font-weight: bold;">MOBILE</th>';
        $str.='<th class="text-center" style="font-weight: bold;">VILLAGE</th>';
        $str.='<th class="text-center" style="font-weight: bold;">TERRITORY</th>';
        $str.='<th class="text-center" style="font-weight: bold;">DISTRICT</th>';
        $str.='<th class="text-center" style="font-weight: bold;">HQ</th>';
        $str.='<th class="text-center" style="font-weight: bold;">ACREAGE</th>';
        $str.='<th class="text-center" style="font-weight: bold;">MR</th>';
        $str.='<th class="text-center" style="font-weight: bold;width:10%">DATE</th>';
        $str.='<tr>';
        $str.='</thead>';
        $str.='<tbody>';
       
        foreach($Frmdata as $val){
            $date=date_create($val['CR_DATE']);
            $cr_date=date_format($date,"Y-m-d");
            $str.='<tr>';
            $str.='<td style="width:10%">'.$i.'</td>';
            $str.='<td>'.ucfirst($val['FARMER_NAME']).'</td>';
            $str.='<td>'.ucfirst($val['FATHER_NAME']).'</td>';
            $str.='<td>'.$val['MOBILE_NO'].'</td>';
            $str.='<td>'.ucfirst($val['VILLAGE_NAME']).'</td>';
            $str.='<td>'.ucfirst($val['TERRITORY_NAME']).'</td>';
            $str.='<td>'.ucfirst($val['DISTRICT_NAME']).'</td>';
            $str.='<td>'.ucfirst($val['HQ_NAME']).'</td>';
            $str.='<td>'.ucfirst($val['ACERAGE']).'</td>';
            $str.='<td>'.ucfirst($val['CR_NAME']).'</td>';
            $str.='<td style="width:12%">'.$cr_date.'</td>';
            $str.='</tr>';

        $i++;
        }

        $str.='</tbody>';
        $str.='</table>';
        $str.='</div>';
        echo $str;
      
    }
}


